/* One fourth of a rectangle with an horizontal crack in the middle.  A vertical force pulls the horizontal boundary up */

 nowait; n:=20;m:=5;
border(1,0,1,n) begin 
x:=1-(t-1)^2;y:=0;
end;
border(2,1,2,n) begin 
x:=(t-1)^2+1;y:=0;
end;
border(3,0,1,m) begin 
x:=2;y:=t;
end;
border(4,0,2,10) begin 
x:=2-t;y:=1;
end;
border(5,0,1,m) begin 
x:=0;y:=1-t;
end;
buildmesh(500);
E:= 2.15;
sigma := 0.29;
mu :=E/(2*(1+sigma));
lambda := E*sigma/((1+sigma)*(1-2*sigma));
nu := lambda+sigma;

solve(u,v)
begin
     onbdy(3,5) u=0;
     onbdy(2) v=0;
     onbdy(4) dnu(v) = 1;
     pde(u) -laplace(u)*mu  - 
               dxx(u)*nu-dxy(v)*nu =0;
     pde(v) -laplace(v)*mu  - 
               dyx(u)*nu-dyy(v)*nu =0;
end;
save('u.dta',u);
save('v.dta',v);
plot(u); plot(v);
