h0 := 1.0; h1 := 1.0;
border(2,0,1,4)   begin x:= 10;  y := h0 * t          end;
border(2,0,1,4)   begin x:= 10;  y := h0 + h1 * t     end;
border(4,0,1,4)   begin x:= 0;   y := h0 + h1 * (1-t) end;
border(4,0,1,4)   begin x:= 0;   y := h0 * (1-t)      end;
border(1,0,10,40) begin x:= t;   y := 0               end;
border(3,0,10,40) begin x:= 10 - t; y := h0 + h1      end;
border(0,0,10,40) begin x:= t;   y := h0              end;
buildmesh(800);

t0 = 10;  t1 = 100; 
kappa =0.01 + one(y > h0);
plot(kappa);

solve(v){
  onbdy(4) v=t0;
  onbdy(2) v = t1;
  onbdy(1) dnu(v)=0;
  onbdy(3) dnu(v)=0;
  pde(v) -laplace(v)*kappa+id(v) = 0;
};
plot(v);
