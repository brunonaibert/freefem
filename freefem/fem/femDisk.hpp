// Emacs will be in -*- Mode: c++ -*-
//
// ********** DO NOT REMOVE THIS BANNER **********
//
// SUMMARY: Language for a Finite Element Method
// RELEASE: 2.0     
// USAGE  : You may copy freely these files and use it for    
//          teaching or research. These or part of these may   
//          not be sold or used for a commercial purpose with- 
//          out our consent : fax (33)1 44 27 44 11        
//
// AUTHORS:  C. Prud'homme
// ORG    :          
// E-MAIL :  prudhomm@users.sourceforge.net
//
// ORIG-DATE:     June-94
// LAST-MOD: 27-Oct-01 at 12:18:24 by Christophe Prud'homme
//
// DESCRIPTION:

/*
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/

// DESCRIP-END.
//

#ifndef __Disk_h
#if defined(__GNUG__)
#pragma interface
#endif /* __GNUG__ */
#define __Disk_h

#if defined(HAVE_CONFIG_H)
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <femMisc.hpp>
#include <femMesh.hpp>

namespace fem
{
  /*!
    \class fcts
    \brief provide a pool for all the PDE data
    \author Christophe Prud'homme <prudhomm@users.sourceforge.net>
    \version $Id: femDisk.hpp,v 1.2 2001/10/28 14:51:18 prudhomm Exp $
  */
  class fcts
  {
  public:   
    float *c1, *b1, *nuyx1,*nuxx1,*nuxy1,*nuyy1, *a21, *a11, *g1, *f1, *p1, *sol1, *fplot;
    ccomplex *c1c, *b1c, *nuyx1c,*nuxx1c,*nuxy1c,*nuyy1c, *a21c, *a11c, *g1c, *f1c, *p1c, *sol1c;
    Acmat c2, b2, nuyx2,nuxx2,nuxy2,nuyy2, a12, a22;
    Acvect g2, p2, f2, sol2;
  
    fcts()
      :
      c1(NULL),
      b1(NULL),
      nuyx1(NULL),
      nuxx1(NULL),
      nuxy1(NULL),
      nuyy1(NULL),
      a21(NULL),
      a11(NULL),
      g1(NULL),
      f1(NULL),
      p1(NULL),
      sol1(NULL),
      fplot(NULL),
      c2(),
      b2(),
      nuyx2(),
      nuxx2(),
      nuxy2(),
      nuyy2(),
      a12(),
      a22(),
      g2(),
      p2(),
      f2(),
      sol2()
    {}
  };
  //! read the .pde file and return the associated buffer
  /*! \fn char* readprog (char *path)
    read the .pde file and return the associated buffer
    \param path	the filename
    \return the buffer
  */
  char* readprog (char *path);

  //! load the triangulation in memory
  /*! \fn int loadtriangulation(femMesh *, char *);
    <long-description>

    \param femMesh * the mesh structure
    \param  char *   the filename
    \return 0 if error 1 otherwise
  */
  int loadtriangulation(femMesh *, char *); 
  
  int savetriangulation(femMesh *, char *);
  int saveparam(fcts *param, femMesh* t, char *path, int N=1);
  int savefct(creal *, int, char *);
  int saveconst(creal, char *);
  int loadsol(int, fcts *);
  int loadfct(creal *, int, char *);
  int readpoints(char *, float*, int);
}
#endif /* __Disk_h */
