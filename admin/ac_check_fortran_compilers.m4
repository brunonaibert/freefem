dnl -*- Mode: m4 -*-
dnl
dnl SUMMARY:      
dnl
dnl AUTHOR:       Christophe Prud'homme
dnl ORG:          Christophe Prud'homme
dnl E-MAIL:       Christophe.Prudhomme@ann.jussieu.fr
dnl
dnl ORIG-DATE:     9-Jan-99 at 14:28:00
dnl LAST-MOD:     13-Jun-00 at 17:52:36 by Christophe Prud'homme
dnl
dnl DESCRIPTION:  
dnl This program is free software; you can redistribute it and/or modify
dnl  it under the terms of the GNU General Public License as published by
dnl  the Free Software Foundation; either version 2 of the License, or
dnl  (at your option) any later version.
dnl  
dnl  This program is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl  GNU General Public License for more details.
dnl
dnl  You should have received a copy of the GNU General Public License
dnl  along with this program; if not, write to the Free Software
dnl  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
dnl
dnl DESCRIP-END.

AC_DEFUN(AC_CHECK_FORTRAN_COMPILERS,
[
  AC_MSG_CHECKING(for a F77-Compiler)
  dnl if there is one, print out. if not, don't matter
  AC_MSG_RESULT($F77) 
 
  if test -z "$F77"; then AC_CHECK_PROG(F77, f90, f90) fi
  if test -z "$F77"; then AC_CHECK_PROG(F77, f77, f77) fi
  if test -z "$F77"; then AC_CHECK_PROG(F77, g77, g77) fi
  test -z "$F77" && AC_MSG_ERROR([no acceptable fortran compiler found in \$PATH])
])