#! /bin/sh
#
#   SUMMARY: 
#     USAGE:
#
#    AUTHOR: Christophe Prud'homme <prudhomm@mit.edu>
#       ORG: MIT
#    E-MAIL: prudhomm@mit.edu
#
# ORIG-DATE: 28-Oct-01 at 10:01:35
#  LAST-MOD: 20-Aug-03 at 09:56:02 by Christophe Prud'homme
#
# RCS Infos:
# ==========
#    Author: $Author: prudhomm $
#        Id: $Id: autogen.sh 206 2006-07-30 16:52:02Z prudhomm $
#  Revision: $Revision: 206 $
#      Date: $Date: 2006-07-30 18:52:02 +0200 (Sun, 30 Jul 2006) $
#    locker: $Locker:  $
#
#
# DESCRIPTION:
# ============
# Distributed under the GPL(GNU Public License):
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
# DESCRIP-END.
#
echo "aclocal -I admin ..." && aclocal -I admin
echo "autoheader..." && autoheader
echo "automake..." && automake
echo "autoconf..." && autoconf

