dnl -*- Mode: m4 -*-
dnl
dnl SUMMARY:      
dnl
dnl AUTHOR:       Christophe Prud'homme
dnl ORG:          Christophe Prud'homme
dnl E-MAIL:       Christophe.Prudhomme@ann.jussieu.fr
dnl
dnl ORIG-DATE:     8-Jun-00 at 18:06:00
dnl LAST-MOD:     13-Jun-00 at 17:50:51 by Christophe Prud'homme
dnl
dnl DESCRIPTION:  
dnl This program is free software; you can redistribute it and/or modify
dnl  it under the terms of the GNU General Public License as published by
dnl  the Free Software Foundation; either version 2 of the License, or
dnl  (at your option) any later version.
dnl  
dnl  This program is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl  GNU General Public License for more details.
dnl
dnl  You should have received a copy of the GNU General Public License
dnl  along with this program; if not, write to the Free Software
dnl  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
dnl DESCRIP-END.

AC_DEFUN(KFEM_CHECK_KDE,
[AC_MSG_CHECKING([kde])
 ac_kde_include=kfile.h
 ac_kde_lib=libkdecore.so

 for ac_dir in \
  /usr/ \
  ;
 do
   if test -r "$ac_dir/include/kde/$ac_kde_include"; then
    ac_kde_includes=$ac_dir/include/kde
    if test -r "$ac_dir/lib/$ac_kde_lib"; then
     ac_kde_libs=$ac_dir/lib/
     break
    fi
    break
   fi
 done
 if test "$ac_kde_libs" = "" -o "$ac_kde_includes" = "";then
   AC_MSG_RESULT([no])
 fi 
 AC_MSG_RESULT([yes])
 CPPFLAGS="${CPPFLAGS} -I$ac_kde_includes "
 if ! test "$ac_kde_libs" = ""; then
   LDFLAGS="${LDFLAGS} -L$ac_kde_libs"
 fi 
 KDELIBS="-lkio -lkdeui -lkdecore"
 AC_SUBST(KDELIBS)
 ]
 AC_DEFINE(HAVE_KDE))
